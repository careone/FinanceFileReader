/***********************************************************************
*Copyright [2018] [Liudewei(793554262@qq.com)]
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
*    http:*www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
************************************************************************/
#ifndef PUBLICDEFINE_H
#define PUBLICDEFINE_H
#include <QString>

#define VERSION_V "V1.9.22"
#define AUTHOR_PHONE "MTUyMzg4NzIxMDE="
#define AUTHOR_NAME "NzkzNTU0MjYyQHFxLmNvbSjliJjlvrfkvY0p"
#define AUTHOR_EMAIL "NzkzNTU0MjYyQHFxLmNvbQ=="
#define AUTHOR_INFO "5L2c6ICFOuWImOW+t+S9jSjlubvoiJ7lpYflvbEpCgoyMDE15bm05q+V5Lia5LqO6YOR5bee5aSn5a2mCg=="
#define WEBSITE_INFO "https://www.ffreader.cn/?from=ffreader"
#define MANUAL_INFO "https://www.ffreader.cn/manual?from=ffreader"
//二维码分享功能开关--严格的生产环境限制数据导出时,需要使用限制分享的版本
#define ENABLE_QRCODE true
#define ALL_CHECK_INDEX 999999

#endif // PUBLICDEFINE_H
