/***********************************************************************
*Copyright [2018] [Liudewei(793554262@qq.com)]
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
*    http:*www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
************************************************************************/
#ifndef DIALOGABOUTAUTHOR_H
#define DIALOGABOUTAUTHOR_H

#include <QDialog>
#include <QByteArray>
#include "src/publicdefine.h"

namespace Ui {
class DialogAboutAuthor;
}

/**
 * @brief The DialogAboutAuthor class 关于作者
 */
class DialogAboutAuthor : public QDialog
{
    Q_OBJECT

public:
    explicit DialogAboutAuthor(QWidget *parent = nullptr);
    ~DialogAboutAuthor();

private slots:
    void on_pushButton_clicked();

private:
    Ui::DialogAboutAuthor *ui;
    int number=0;
};

#endif // DIALOGABOUTAUTHOR_H
